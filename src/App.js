import React, { Component } from 'react';
import { ToDoListRedux, store } from './Components/ToDoList.js';
import { Provider } from "react-redux";

class App extends Component {
  render() {
    return (
      <Provider store={store}>
          <ToDoListRedux/>
      </Provider>
    );
  }
}

export default App;
