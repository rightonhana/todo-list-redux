import React from "react";
import { createStore } from "redux";
import { connect } from "react-redux";
import { ToDoAdd } from "./ToDoAdd";
import { ToDoItems } from "./ToDoItems";

export const ToDoList = ({items, onAdd, onDone, onDelete}) => (
    <div>
        <ToDoAdd onAdd={onAdd}/>
        <ToDoItems items={items} onDone={onDone} onDelete={onDelete}/>
    </div>
);

const defaultState = {
    items: []
};

const actions = {
    add: {type: "add"},
    done: {type: "done"},
    delete: {type: "delete"}
};

const reducers = {
    add:(state, {value}) => ({
        items: [ ...state.items, {
            value: value,
            done: false
        } ]
    }),
    done: (state, {index}) => ({
        items: [
            ...state.items.slice(0, index),
            {
                ...state.items[index],
                done: !state.items[index].done
            },
            ...state.items.slice(index + 1)
        ]
    }),
    delete: (state, {index}) => ({
        items: [
            ...state.items.slice(0, index),
            ...state.items.slice(index+1)
        ]
    })
};

const mainReducer = (state = defaultState, action) =>
    reducers[action.type] 
        ?
            {
            ...state,
            ...reducers[action.type](state, action)
            }
        : state;
    

export const store = createStore(mainReducer);

const mapStateToProps = state => ({
    items: state.items
});

const mapDispatchToProps = dispatch => ({
    onAdd: (value) => dispatch({ ...actions.add, value }),
    onDone: (index) => dispatch({...actions.done, index}),
    onDelete: (index) => dispatch({...actions.delete, index})
});

export const ToDoListRedux = connect(mapStateToProps, mapDispatchToProps)(ToDoList);