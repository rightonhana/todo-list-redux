import React from "react";

export const ToDoAdd = ({ onAdd }) => {
    let input;
    const onSubmit = event => {
        event.preventDefault();
        onAdd(input.value);
        input.value = "";
    };

    return (
        <form onSubmit={onSubmit}> 
            <input type="text" ref={inputRef => input = inputRef}/>
            <button type="submit">Add</button>
        </form> 
    );
};